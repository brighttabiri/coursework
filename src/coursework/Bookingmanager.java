/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package coursework;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 *This class will be used to store all the group exercise lesson on Saturday.
 * 
 * The class has an array list "lessons" to store all the exercise on the day.
 * 
 * @author brighttabiri
 */
public final class Bookingmanager {
    
    


 private  ArrayList<Student>  allstudents;
 private  ArrayList<Dateoflesson>  alldates;
 private  ArrayList<Dateoflesson>  specdates;
 private  ArrayList<Booking>  bookings;
 private  ArrayList<Lesson>  allessons;
 private  ArrayList<Booking>  attendedbookings;
 Lesson lesson;
 Student stu;
 private static Scanner reader= new Scanner(System.in);

 private String namme;
 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    
    
    
  Lesson lesson1 ;
  Lesson lesson2 ;
  Lesson lesson3;
  Lesson lesson4;
 
public  Bookingmanager (){
  
    
   
   
    loadstudents();
  
    loadDate();
    reader= new Scanner(System.in);
    bookings = new ArrayList<>();
      allessons = new ArrayList<>();
      specdates = new ArrayList<>();
    
    attendedbookings = new ArrayList();
        
         
} 

// Adds a student to the allstudents array
public void addastudent(Student st){
        allstudents.add(st);
  }
public int ddstudent(){
        return bookings.size();
  }
public int ddestudent(){
        return attendedbookings.size();
  }
//Adds a date of lesson to the alldates array
public void addadates(Dateoflesson dt){   
        alldates.add(dt);
  }


    
     //Gets all the current size of the allstudents array     
 public int getallstudentsize(){
        return allstudents.size();
        }
   
    /** returns a String representation of the lesson in the position given 
     * by the parameter
     * @param position
     * @return 
     */
 
//Displays all lessons on the calendar with their dates
public void showAlllessons(){
    System.out.println("LESSONS SCHEDULE\n");
        int index = 0;       
        while (index < alldates.size()) 
        {   
            Dateoflesson temp = alldates.get(index);
              temp.printlessons();
            index++;
        }
        
        }

   
   //Function to find a student
     public Boolean findstudent(String id)
    {
       
            for(int z = 0; z < allstudents.size(); z++) {
                if (allstudents.get(z).getid().equalsIgnoreCase(id)) {
                    stu = allstudents.get(z);
                    //System.out.print("student: " + z +"\n "+"Details:"+ stu.toString());
                    return true;
                }
            }
        
        return false;

    }
    
     //Method to make a booking
      public String makeabooking()
    {
  
      
        
        
       Booking newbooking = new Booking(stu,lesson);
       bookings.add(newbooking);
       lesson.addastudenttolesson(stu);
       System.out.print(newbooking.returnbookingid());
       System.out.print(newbooking.returnlesson().toString());
       
       return "Booking susesful";
       
        
        //return "Booking unsuccesful";
    }
       public Boolean checklessoncapacity()
    {
          for(int i =0; i < bookings.size();i++){
       if(bookings.get(i).returnlesson().returnstudentcount()<4){
           return true;
       }}
          return false;
    }
      
       //Method to make a booking
      public String deleteabooking(String id)
    {
       for(int i = 0; i<bookings.size();i++){
           if(bookings.get(i).returnbookingid().equalsIgnoreCase(id)){
           bookings.remove(i);
           System.out.println(ddstudent());
           return "Booking deleted sucessfully";
           
           }
       }
       return "Booking not found";
    }
      //Method to attend a booking
         public String attendlesson(Attendlesson at,String id)
    {
       for(int i = 0; i<bookings.size();i++){
           if(bookings.get(i).returnbookingid().equalsIgnoreCase(id)){
            bookings.get(i).setatle(at);
           attendedbookings.add(bookings.get(i));
           bookings.remove(i);
           
           System.out.println(ddstudent());
           System.out.println(ddestudent());
           return "Booking attended sucessfully";
           
           }
       }
       return "Booking not found";
    }
      
   
      
      //Find a booking using the bokking id
         public Boolean findabooking(String id)
    {
       for(int i = 0; i<bookings.size();i++){
           if(bookings.get(i).returnbookingid().equalsIgnoreCase(id)){
           
           System.out.println(ddstudent());
           return true;
           
           }
       }
       return false;
    }
      
     //prints a lesson using the lesson's name
    public Boolean printalesson(String name )  
    {
         Boolean b= false; 
        for(int x =0; x<alldates.size();x++){
           
        
        
         for (int i =0; i <alldates.get(x).getles().size();i++){
               if(alldates.get(x).getles().get(i).getName().equalsIgnoreCase(name)){
                   specdates.add(alldates.get(x));
                   LocalDate d =alldates.get(x).getdate();
                   Lesson l = alldates.get(x).getles().get(i);
              String dateString = d.format(formatter);
                   String lname = l.getName();
          
       System.out.print(lname+" is on "+dateString +"\n");
        b =true;
        } 
                namme = name; 
               // System.out.print(namme);
        }
   
        }
             return b;
        //System.out.print(namme);
        }
          
     //checks for a specific lesson using the lesson's date     
    public Boolean printspeclesson(String dateoflesson )
             
    {
        Boolean b= false;
        for(int i = 0; i < specdates.size();i++){
            for(int z =0; z < specdates.get(i).getles().size();z++){
        if(specdates.get(i).getAsString().equalsIgnoreCase(dateoflesson)&&specdates.get(i).getles().get(z).getName().equalsIgnoreCase(namme)){
            LocalDate specdate= specdates.get(i).getdate();
             lesson = specdates.get(i).getles().get(z);
            String lname = lesson.getName();
         System.out.print(lname);
         b=true;
        
        } 
            }
              
        }
         return b;
    }
                

      
    //prints all students and return the studentlist 
   public ArrayList getstudents(ArrayList<Student> students){
 
        int index = 0;       
        while (index < allstudents.size()) 
        { 
          // ArrayList<Student> students =new ArrayList<Student>();;
            
            students.add(allstudents.get(index)); 
           
            //stu = students;
            System.out.println(students);
            index++;
        }
   return students;
   }
   
   
   //Prints all students in the stystem
   public void printstudents(){
        int index = 0;       
         while (index < allstudents.size()) 
        { 
        System.out.println(allstudents.get(index).toString()); 
            //System.out.println(allstudents.get(index).toString());
            index++;
        }
     
   }
   
   //Prints all lessons to aid user
     public void printlessons(){
         for (int i =0; i< allessons.size();i++){
         
         System.out.println(allessons.get(i).getName());
         }
      
       
      // System.out.print(l1.getlessnameAsString());
   }
     
   //Creates all students and store them
   public void loadstudents(){
    allstudents = new ArrayList<>();

   
   Student st1 = new Student("Beatrice Oppong", "101");
   allstudents.add(st1);
   
   Student st2 = new Student("Nancy Mamme", "102");
   allstudents.add(st2);
   
   Student st3 = new Student("Kwadwo Yeboah", "103");
   allstudents.add(st3);
   
   Student st4 = new Student("Akrobeto Etwe", "104");
        allstudents.add(st4);
        
    Student st5 = new Student("Mamme Twe", "105");
  allstudents.add(st5);
  
   Student st6 = new Student("Cynthia Akosua", "106");
   allstudents.add(st6);
   
   Student st7 = new Student("Emmanuella Nyantakyiwaa", "107");
   allstudents.add(st7);
   
   Student st8 = new Student("Sobolo Nicca", "108");
   allstudents.add(st8);
   
   Student st9 = new Student("Khalifa Realest", "109");
        allstudents.add(st9);
        
         Student st10 = new Student("Kwabena Mason", "110");
        allstudents.add(st10);
        
        
        
        } 
         
         
         
      //Create all the dates of lessons and date them to the alldates collection with their lessons  
     public void loadDate(){
         allessons = new ArrayList<>();
    
         Lesson l1 = new Lesson ("Gym");
     allessons.add(l1);
      Lesson l2 = new Lesson ("Boxing");
      allessons.add(l2);
       Lesson l3 = new Lesson ("Running");
        allessons.add(l3);
        Lesson l4= new Lesson ("fitbizz");
        allessons.add(l4);
               
       
      alldates = new ArrayList<>();
        Dateoflesson firstweekendsat = new Dateoflesson (16,04,2022);
        alldates.add(firstweekendsat);
        firstweekendsat.addalessontodate(l1,l3,l4);
        
        
         Dateoflesson firstweekendsun = new Dateoflesson (17,04,2022);
        alldates.add(firstweekendsun);
        firstweekendsun.addalessontodate(l2,l3,l1);
       
        Dateoflesson secondweekendsat = new Dateoflesson (23,04,2022);
        alldates.add(secondweekendsat);
        secondweekendsat.addalessontodate(l4, l3, l1);
        
        Dateoflesson secondweekendsun = new Dateoflesson (24,04,2022);
        alldates.add(secondweekendsun);
        secondweekendsun.addalessontodate(l4, l2, l3);
        
        Dateoflesson thirdweekendsat = new Dateoflesson (30,04,2022);
        alldates.add(thirdweekendsat);
        thirdweekendsat.addalessontodate(l4, l3, l1);
        
          Dateoflesson thirdweekendsun = new Dateoflesson (01,05,2022);
        alldates.add(thirdweekendsun);
        thirdweekendsun.addalessontodate(l3, l2, l1);
        
       
        Dateoflesson fourthweekendsat = new Dateoflesson (07,05,2022);
        alldates.add(fourthweekendsat );
        fourthweekendsat.addalessontodate(l4, l2, l3);
        
         Dateoflesson fourthweekendsun = new Dateoflesson (8,05,2022);
        alldates.add(fourthweekendsun);
        fourthweekendsun.addalessontodate(l4, l3, l1);
        
        Dateoflesson fifthweekendsat = new Dateoflesson (14,05,2022);
         alldates.add(fifthweekendsat);
         fifthweekendsat.addalessontodate(l1, l2, l3);
         
         Dateoflesson fifthweekendsun = new Dateoflesson (15,05,2022);
         alldates.add(fifthweekendsun);
          fifthweekendsun.addalessontodate(l4, l2, l3);
         
        Dateoflesson sixthweekendsat = new Dateoflesson (21,05,2022);
         alldates.add(sixthweekendsat);
          sixthweekendsat.addalessontodate(l2, l4, l1);
         
         Dateoflesson sixthweekendsun = new Dateoflesson (22,05,2022);
         alldates.add(sixthweekendsun);
         sixthweekendsun.addalessontodate(l4, l1, l3);
         
        Dateoflesson seventhweekendsat = new Dateoflesson (28,05,2022);
        alldates.add(seventhweekendsat);
        seventhweekendsat.addalessontodate(l3, l2, l1);
        
         Dateoflesson seventhweekendsun = new Dateoflesson (29,05,2022);
        alldates.add(seventhweekendsun);
        seventhweekendsun.addalessontodate(l4, l1, l2);
        
      Dateoflesson eightthweekendsat = new Dateoflesson (04,06,2022);
      alldates.add(eightthweekendsat);
      eightthweekendsat.addalessontodate(l1, l2, l3);
      
      Dateoflesson eigthweekendsun = new Dateoflesson (05,06,2022);
      alldates.add(eigthweekendsun);
      eigthweekendsun.addalessontodate(l2, l4, l1);
        } 
           
           
           
           
              
}
