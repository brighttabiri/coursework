/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package coursework;
import java.util.*;
/**
 *
 * @author brighttabiri
 */
public class Booking {
    
    Bookingmanager db;
 
     private Student student;
    private Lesson Lesson;
    private String bookingid;
    Attendlesson atle;
    
    //Creating a booking object and using the student's id to set the booking id
    
    public Booking(Student st, Lesson le){
       
  student = st;
  Lesson = le;
  String id = st.getid();
    bookingid = id;
    
    
    }
    //Returns student in booking
    public Student returnstudent(){
    return student;}
    
    //Returns lesson in booking
      public Lesson returnlesson(){
    return Lesson;}
      
      //Returns booking id in booking
     public String returnbookingid(){
    return bookingid;}
     
     public Attendlesson returnattendbookreview(){
    return atle;}
    
     public void setatle(Attendlesson at){
    
     atle = at;
     }
    
}
