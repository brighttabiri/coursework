package coursework;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Simple Date Class - no error checking.
 * 
 * @author A.A.Marczyk (based on earlier version by M.Wood)
 * @version Version 1.0 - 15/11/04
 */
public class Dateoflesson
{
   
    /** Fields of a Date - just the day, month and year*/
    private int day;
    private int month;
    private int year;
    private Calendar c;
    private Date Dateoflesson;
  private ArrayList<Lesson> lessons;
  LocalDate dat;
   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    
    
    /**
     * Constructor for objects of class Date
     * @param d - the day part of the date (1 - 31, depending on the month).
     * @param m - the month part of the date (1 - 12).
     * @param y - the year part of the date.
     */
    public Dateoflesson(int d, int m, int y)
    {
        LocalDate da ;
        //=new LocalDate();;
         
       lessons = new ArrayList<Lesson>();
   
       
         day = d;
         month = m;
         year = y;
         //da= new LocalDate(y, m, d);
       
       dat = LocalDate.of(year, month, day);
      
            
 // dat.getTime();
    }

    Dateoflesson(int i) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    /**
     * resets the date to the specified parameters
     * @param d - the day part of the date (1 - 31, depending on the month).
     * @param m - the month part of the date (1 - 12).
     * @param y - the year part of the date.
     */
    public void resetDate(int d, int m, int y)
    {
        day = d;
        month = m;
        year = y;
             dat = LocalDate.of(year, month, day);
    }
    //Get day of lesson
     public int returndayofless()
    {
   return day;    
    }
    //Add a lesson ta a dat of lesson
     public void addalessontodate(Lesson mon,Lesson aff,Lesson eve)
    {
       lessons.add(mon);
       lessons.add(aff);
       lessons.add(eve);
       //Dateoflesson = mon.
       
    }
     
     //Display all lesson on a date
     public void printlessons()
             
    {
       String dateString = dat.format(formatter);
        System.out.println("""
                           
                           *********
                           """+dateString+"\n"+"*********");
        for(int i =0; i < lessons.size(); i++){
        Lesson l = lessons.get(i);
        String lname = l.getName();
       System.out.print(lname+"\n");
        
        
        }
    }
     
     // prints a lesson using the lesson's name
          public void printalesson(String name )
             
    {
        for(int x =0; x<lessons.size();x++){
        if (lessons.get(x).getName().equalsIgnoreCase(name)){
        String dateString = dat.format(formatter);
        System.out.print(dateString+"\n");
        
        Lesson l = lessons.get(x);
        String lname = l.getName();
       System.out.print(lname+"\n");
        }
        else{
        System.out.println("Notfound");}
        
        }
      
    }
     
     //Prints date in a string
    /*public void printdate (){
        String dateString = dat.format(formatter);
    System.out.print(dateString);}*/
    
    
    /** returns the year
     * @return year
     */
    public int getYear()
    {
        return year;
    }
    public int getmonth()
    {
        return month;
    }
     public LocalDate getdate()
    {
        return dat;
    }
     
     //Gets all lessons on a date
      public ArrayList<Lesson> getles()
    {
        return lessons;
    }
    

    /**
     * @return the date as a String, format "09/11/2002"
     */
    public String getAsString ()
    {
        return as2Digits(day) + "/" + as2Digits(month) + "/" + year;
    }
    
    /** Internal method to add a leading zero if necessary. */
    private String as2Digits (int i)
    {
        if (i <10) 
        {
            return "0" + i;
        }
        else 
        {
            return "" + i;
        }
    }
}
